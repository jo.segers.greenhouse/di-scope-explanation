﻿using System;
using System.Text;
using di_scope_explanation.Tutorial.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace di_scope_explanation.Controllers {
  [ApiController]
  [Route("[controller]")]
  public class TutorialController : ControllerBase {
    private readonly ITutorialService _TutorialService1;
    private readonly ITutorialService _TutorialService2;
    private readonly IServiceScopeFactory _ServiceScopeFactory;

    public TutorialController(ITutorialService tutorialService1, ITutorialService tutorialService2, IServiceScopeFactory serviceScopeFactory) {
      _TutorialService1 = tutorialService1;
      _TutorialService2 = tutorialService2;
      _ServiceScopeFactory = serviceScopeFactory;
    }
    
    [HttpGet]
    public IActionResult Get() {
      var output = new StringBuilder();
      var requestTime = DateTime.Now;
      
      // Basic stuff
      
      output.AppendLine(_TutorialService1.OutputIds(nameof(_TutorialService1), requestTime));
      output.AppendLine(_TutorialService2.OutputIds(nameof(_TutorialService2), requestTime));

      
      // Advanced stuff: using custom scopes
      
      using (var scope1 = _ServiceScopeFactory.CreateScope()) {
        var tutorialService1InScope1 = scope1.ServiceProvider.GetService<ITutorialService>();
        var tutorialService2InScope1 = scope1.ServiceProvider.GetService<ITutorialService>();

        output.AppendLine(tutorialService1InScope1.OutputIds(nameof(tutorialService1InScope1), requestTime));
        output.AppendLine(tutorialService2InScope1.OutputIds(nameof(tutorialService2InScope1), requestTime));
      }
      
      using (var scope2 = _ServiceScopeFactory.CreateScope()) {
        var tutorialService1InScope2 = scope2.ServiceProvider.GetService<ITutorialService>();
        var tutorialService2InScope2 = scope2.ServiceProvider.GetService<ITutorialService>();

        output.AppendLine(tutorialService1InScope2.OutputIds(nameof(tutorialService1InScope2), requestTime));
        output.AppendLine(tutorialService2InScope2.OutputIds(nameof(tutorialService2InScope2), requestTime));
      }
      
      return Ok(output.ToString());
    }
  }
}
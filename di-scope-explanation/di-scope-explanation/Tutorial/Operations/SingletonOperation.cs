﻿using di_scope_explanation.Tutorial.Operations.Interface;

namespace di_scope_explanation.Tutorial.Operations {
  public class SingletonOperation : BaseOperation, ISingletonOperation {
    
  }
}
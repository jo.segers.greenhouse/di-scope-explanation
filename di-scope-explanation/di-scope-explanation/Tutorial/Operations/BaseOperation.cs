﻿using System;

namespace di_scope_explanation.Tutorial.Operations {
  public abstract class BaseOperation {
    public Guid Id { get; }

    public BaseOperation() {
      Id = Guid.NewGuid();
    }
  }
}
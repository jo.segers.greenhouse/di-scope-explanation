﻿using System;

namespace di_scope_explanation.Tutorial.Operations.Interface {
  public interface IBaseOperation {
    Guid Id { get; }
  }
}
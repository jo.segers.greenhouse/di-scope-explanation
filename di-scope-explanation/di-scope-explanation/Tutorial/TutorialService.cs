﻿using System;
using di_scope_explanation.Tutorial.Interface;
using di_scope_explanation.Tutorial.Operations.Interface;

namespace di_scope_explanation.Tutorial {
  public class TutorialService : ITutorialService {
    private readonly ITransientOperation _TransientOperation;
    private readonly IScopedOperation _ScopedOperation;
    private readonly ISingletonOperation _SingletonOperation;
    
    public TutorialService(ITransientOperation transientOperation, IScopedOperation scopedOperation, ISingletonOperation singletonOperation) {
      _TransientOperation = transientOperation;
      _ScopedOperation = scopedOperation;
      _SingletonOperation = singletonOperation;
    }

    public string OutputIds(string serviceName, DateTime requestTime) {
      var output = $@"----------
ServiceName: {serviceName} RequestTime: {requestTime}
transientOperation id: {_TransientOperation.Id}
scopedOperation id: {_ScopedOperation.Id}
singletonOperation id: {_SingletonOperation.Id}
----------";
      
      Console.WriteLine(output);

      return output;
    }
  }
}
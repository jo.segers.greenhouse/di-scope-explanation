﻿using System;

namespace di_scope_explanation.Tutorial.Interface {
  public interface ITutorialService {
    string OutputIds(string serviceName, DateTime requestTime);
  }
}